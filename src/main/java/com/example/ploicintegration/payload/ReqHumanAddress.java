package com.example.ploicintegration.payload;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ReqHumanAddress {
    @JsonProperty("sCountriesId")
    private Integer sCountriesId;
    @JsonProperty("sOblastiId")
    private Integer sOblastiId;
    @JsonProperty("sRegionId")
    private Integer sRegionId;
    @JsonProperty("sSettlementId")
    private Integer sSettlementId;
    @JsonProperty("sMahallyaId")
    private Integer sMahallyaId;
    private String street;
    private String house;
    private String flat;
    @JsonProperty("sNote")
    private String sNote;
}
