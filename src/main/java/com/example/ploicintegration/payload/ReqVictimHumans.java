package com.example.ploicintegration.payload;

import com.example.ploicintegration.entity.VictimAddress;
import lombok.Data;

import java.util.Date;

@Data
public class ReqVictimHumans {
    private Integer victimId;
    private String lastName;
    private String firstName;
    private String middleName;
    private Date dateOfBirth;
    private String phone;
    private String sex;
    private String placeOfBirth;
    private String sOccupationId;
    private String exactBusiness;
    private String victimType;
    private VictimAddress victimAddress;
}
