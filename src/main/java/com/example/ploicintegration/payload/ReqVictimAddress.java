package com.example.ploicintegration.payload;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ReqVictimAddress {
    @JsonProperty("sCountriesId")
    private Long sCountriesId;
    @JsonProperty("sOblastiId")
    private Long sOblastiId;
    @JsonProperty("sRegionId")
    private Long sRegionId;
    @JsonProperty("sSettlementId")
    private Long sSettlementId;
    @JsonProperty("sMahallyaId")
    private Long sMahallyaId;
    private String street;
    private String house;
    private String flat;
    @JsonProperty("sNote")
    private String sNote;
}
