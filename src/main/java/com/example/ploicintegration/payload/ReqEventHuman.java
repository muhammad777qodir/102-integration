package com.example.ploicintegration.payload;

import lombok.Data;

@Data
public class ReqEventHuman {
    private Integer humanId;
    private String lastName;
    private String firstName;
    private String middleName;
    private String phone;
    private Integer hospital;
    private Integer hospitaldept;
    private Integer treatmentkind;
    private Integer professionidcaller;
    private String checkin;
    private ReqHumanAddress humanAddress;
}
