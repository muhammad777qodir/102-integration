package com.example.ploicintegration.payload;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class Response {
    private Integer cardId;
    private Integer branchId;
    private String userFio;
    private Date dateCreateCard;
    private Float geoLatitude;
    private Float geoLongitude;
    private Integer firstOfAll;
    private Boolean hospitalApplication;
    private Integer initSeventFormsId;
    private Integer sEventFormsAddId;
    private Integer gomNum;
    private Date eventStart;
    private Date eventEnd;
    private Integer traumaQuantity;
    private Integer deadQuantity;
    private String fabula;
    private ReqEventAddress eventAddress;
    private ReqEventHuman eventHuman;
    private List<ReqVictimHumans> victimHumans;

    @Override
    public String toString() {
        return "Response{" +
                "cardId='" + cardId + '\'' +
                ", branchId='" + branchId + '\'' +
                ", userFio='" + userFio + '\'' +
                ", dateCreateCard='" + dateCreateCard + '\'' +
                ", geoLatitude='" + geoLatitude + '\'' +
                ", geoLongitude='" + geoLongitude + '\'' +
                ", firstOfAll='" + firstOfAll + '\'' +
                ", hospitalApplication='" + hospitalApplication + '\'' +
                ", initSeventFormsId='" + initSeventFormsId + '\'' +
                ", sEventFormsAddId='" + sEventFormsAddId + '\'' +
                ", gomNum='" + gomNum + '\'' +
                ", eventStart='" + eventStart + '\'' +
                ", eventEnd='" + eventEnd + '\'' +
                ", traumaQuantity='" + traumaQuantity + '\'' +
                ", deadQuantity='" + deadQuantity + '\'' +
                ", fabula='" + fabula + '\'' +
                ", eventAddress=" + eventAddress +
                ", eventHuman=" + eventHuman +
                ", victimHumans=" + victimHumans +
                '}';
    }
}
