package com.example.ploicintegration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PloicIntegrationApplication {

    public static void main(String[] args) {
        SpringApplication.run(PloicIntegrationApplication.class, args);
    }

}
