package com.example.ploicintegration.entity;

import com.example.ploicintegration.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EventHuman extends AbsEntity {
    private Integer humanId;
    private String lastName;
    private String firstName;
    private String middleName;
    private String phone;
    private Integer hospital;
    private Integer hospitaldept;
    private Integer treatmentkind;
    private Integer professionidcaller;
    private String checkin;

    @ManyToOne
    private HumanAddress humanAddress;
}
