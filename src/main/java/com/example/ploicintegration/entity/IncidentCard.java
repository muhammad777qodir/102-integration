package com.example.ploicintegration.entity;

import com.example.ploicintegration.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class IncidentCard extends AbsEntity {
    @Column(nullable = false)
    private Integer cardId;
    @Column(nullable = false)
    private Integer branchId;
    @Column(nullable = false)
    private String userFio;
    @Column(nullable = false)
    private Date dateCreateCard;
    private Double latitude;
    private Double longitude;
    @Column(nullable = false)
    private Integer FirstOfAll;
    @Column(nullable = false)
    private Boolean hospitalApplication;
    @Column(nullable = false)
    private Integer initSevenFormsId;
    private Integer sEventFormsAddId;
    @Column(nullable = false)
    private Number gomNum;
    @Column(nullable = false)
    private Date eventStart;
    @Column(nullable = false)
    private Date eventEnd;
    @Column(nullable = false)
    private Number traumaQuantity;
    private Number deadQuantity;
    @Column(nullable = false)
    private String fabula;

    @ManyToOne
    private EventAddress eventAddress;

    @ManyToOne
    private EventHuman eventHuman;

    @OneToMany
    private List<VictimHumans> victimHumans;

}
