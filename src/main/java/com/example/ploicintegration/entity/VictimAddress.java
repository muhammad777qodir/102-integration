package com.example.ploicintegration.entity;

import com.example.ploicintegration.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class VictimAddress extends AbsEntity {
    private Integer sCountriesId;
    private Integer sOblastiId;
    private Integer sRegionId;
    private Integer sSettlementId;
    private Integer sMahallyaId;
    private String street;
    private String sNote;
    private String house;
    private String flat;
}
