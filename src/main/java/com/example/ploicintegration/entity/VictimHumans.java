package com.example.ploicintegration.entity;

import com.example.ploicintegration.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class VictimHumans extends AbsEntity {
    @Column(nullable = false)
    private Integer victimId;
    @Column(nullable = false)
    private String lastName;
    @Column(nullable = false)
    private String firstName;
    @Column(nullable = false)
    private String middleName;
    @Column(nullable = false)
    private Date dateOfBirt;
    private String phone;
    private String sex;
    private String placeOfBirth;
    private String sOccupationId;
    private String exactBusiness;
    private String victimType;

    @ManyToOne
    private VictimAddress victimAddress;
}
