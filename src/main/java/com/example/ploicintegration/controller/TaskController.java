package com.example.ploicintegration.controller;

import com.example.ploicintegration.payload.Response;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/integrations/kriminalist/card102/updated")
public class TaskController {

    @PostMapping
    public String getData(@RequestBody Response response){
        System.out.println(response);
        return "worked";
    }

}
